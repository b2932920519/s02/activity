//Repositories
//These are commonly used functionalities performed on databases objects.
package com.zuitt.wdc044.repositories;
import com.zuitt.wdc044.models.Post;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;

// an interface contains behavior that class implements
// an interface is marked as @Repository which contains methods for database manipulation.
// by extending CrudRepository class, the PostRepository has inherited its pre-defined methods for creating, retrieving, updating and deleting records.


@Repository
public interface PostRepository extends CrudRepository<Post, Object>{



}
